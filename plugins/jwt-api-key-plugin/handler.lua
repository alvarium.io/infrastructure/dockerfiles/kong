-- Extending the Base Plugin handler is optional, as there is no real
-- concept of interface in Lua, but the Base Plugin handler's methods
-- can be called from your child implementation and will print logs
-- in your `error.log` file (where all logs are printed).
local BasePlugin = require "kong.plugins.base_plugin"
local JwtApiKeyHandler = BasePlugin:extend()
local url = require "socket.url"
local responses = require "kong.tools.responses"
local http = require "resty.http"
local json = require "cjson"
local constants = require "kong.constants"
local jwt_decoder = require "kong.plugins.jwt-api-key-plugin.jwt_parser"

-- Setting auth group priority
JwtApiKeyHandler.PRIORITY = 1000

local HTTPS = "https"

-- Parse url based on http-log plugin
local function parse_url(host_url)
  local parsed_url = url.parse(host_url)
  if not parsed_url.port then
    if parsed_url.scheme == "http" then
      parsed_url.port = 80
     elseif parsed_url.scheme == HTTPS then
      parsed_url.port = 443
     end
  end
  if not parsed_url.path then
    parsed_url.path = "/"
  end
  return parsed_url
end

-- Retrieve a JWT in a request.
-- Checks for the JWT in URI parameters, then in the `Authorization` header.
-- @param request ngx request object
-- @param conf Plugin configuration
-- @return token JWT token contained in request or nil
-- @return err
local function retrieve_apikey(request, conf)
  local uri_parameters = request.get_uri_args()

  for _, v in ipairs(conf.uri_param_names) do
    if uri_parameters[v] then
      return uri_parameters[v]
    end
  end

end

local function checkJwtValueMatch(token, config)
  local jwt, err = jwt_decoder:new(token)
  if jwt then
      local claims = jwt.claims
      if not claims[config.match_field] and config.ignore_match_absence then
          return
      end
      if claims[config.match_field] == config.match_value then
          return
      else
          return responses.send_HTTP_UNAUTHORIZED()
      end
  end
end


-- Your plugin handler's constructor. If you are extending the
-- Base Plugin handler, it's only role is to instanciate itself
-- with a name. The name is your plugin name as it will be printed in the logs.
function JwtApiKeyHandler:new()
  JwtApiKeyHandler.super.new(self, "jwt-api-key-plugin")
end

function JwtApiKeyHandler:init_worker(config)
  -- Eventually, execute the parent implementation
  -- (will log that your plugin is entering this context)
  JwtApiKeyHandler.super.init_worker(self)

  -- Access to configuration values
  -- print(config.jwt_server_url) -- {"jwt_server_url"}

  -- Implement any custom logic here
end

function JwtApiKeyHandler:certificate(config)
  -- Eventually, execute the parent implementation
  -- (will log that your plugin is entering this context)
  JwtApiKeyHandler.super.certificate(self)

  -- Implement any custom logic here
end

function JwtApiKeyHandler:access(config)
  -- (will log that your plugin is entering this context)
  JwtApiKeyHandler.super.access(self)

  -- Ignoring this request if tha path is in path_whitelist
  if config.path_whitelist then
    for _, v in ipairs(config.path_whitelist) do
      local regexp = "^" .. v
      ngx.log(ngx.NOTICE, "Matching " .. regexp .. " in " .. ngx.var.request_uri)
      local m, err = ngx.re.match(ngx.var.request_uri, regexp)
      if m then
        ngx.log(ngx.NOTICE, "Path matched whitelist")
        return
      end
    end
  end

  -- Check if the request already has JWT defined in Authorization headers
  local authorization_header = ngx.req.get_headers()["authorization"]
  if authorization_header then
    local iterator, iter_err = ngx.re.gmatch(authorization_header, "\\s*[Bb]earer\\s+(.+)")
    if not iterator then
      return nil, iter_err
    end

    local m, err = iterator()
    if err then
      return nil, err
    end

    if m then
      return
    end
  end


  local ok, err
  local parsed_url = parse_url(config.jwt_server_url)

  -- Getting apikey from querystring or authorization headers
  local apikey, err = retrieve_apikey(ngx.req, config)
  if err then
    return responses.send_HTTP_INTERNAL_SERVER_ERROR(err)
  end
  -- No apikey so unauthorize request
  if not apikey then
    return responses.send_HTTP_UNAUTHORIZED()
  end

  -- local host = parsed_url.host
  -- local port = tonumber(parsed_url.port)

  local endpoint = config.jwt_server_url .. "?apikey=" .. apikey
  local httpc = http.new()

  local res, err = httpc:request_uri(endpoint, {
      method = "GET",
      -- headers = ngx.req.get_headers(0, true),
  })

  if not res then
      ngx.log(ngx.ERR, "An error has occured: ", err)
      return responses.send_HTTP_UNAUTHORIZED()
  end

  local resp = json.decode(res.body)

  if not resp['token'] then
      ngx.log(ngx.ERR, "An error has occured: ", err)
      return responses.send_HTTP_UNAUTHORIZED()
  end

  local ok, err = checkJwtValueMatch(resp['token'], config)

  ngx.req.set_header("Authorization", "Bearer " .. resp['token'])

  local ok, err = http:close()
end

function JwtApiKeyHandler:header_filter(config)
  -- Eventually, execute the parent implementation
  -- (will log that your plugin is entering this context)
  JwtApiKeyHandler.super.header_filter(self)

  -- Implement any custom logic here
end

function JwtApiKeyHandler:body_filter(config)
  -- Eventually, execute the parent implementation
  -- (will log that your plugin is entering this context)
  JwtApiKeyHandler.super.body_filter(self)

  -- Implement any custom logic here
end

function JwtApiKeyHandler:log(config)
  -- Eventually, execute the parent implementation
  -- (will log that your plugin is entering this context)
  JwtApiKeyHandler.super.log(self)

  -- Implement any custom logic here
end

-- This module needs to return the created table, so that Kong
-- can execute those functions.
return JwtApiKeyHandler

