return {
  no_consumer = true, -- this plugin will only be API-wide,
  fields = {
    -- Describe your plugin's configuration's schema here.
    jwt_server_url = {type = "string", required = true, default = "http://auth.alvarium.io/apiToken"},
    timeout = {type = "string", required = true, default = "30"},
    uri_param_names = {type = "array", default = {"apikey"}},
    path_whitelist = {type = "array", default = {}},
    match_field = {type = "string", default = "a field that contains a fixed value for this API"},
    match_value = {type = "string", default = "a value for the `match_field` to match it and block connections missing this value"},
    ignore_match_absence = {type = "boolean", default = true}
  },
  self_check = function(schema, plugin_t, dao, is_updating)
    -- perform any custom verification
    return true
  end
}
