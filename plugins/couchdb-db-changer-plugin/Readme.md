couchdb-database-changer
========================

Kong plugin to retrieve a valid JWT based on provided apikey and then redirect request to its
original endpoint renaming the database requested.

## References

https://getkong.org/docs/0.8.x/plugin-development/
