-- Extending the Base Plugin handler is optional, as there is no real
-- concept of interface in Lua, but the Base Plugin handler's methods
-- can be called from your child implementation and will print logs
-- in your `error.log` file (where all logs are printed).
local BasePlugin = require "kong.plugins.base_plugin"
local CouchDbChangerHandler = BasePlugin:extend()
local url = require "socket.url"
local responses = require "kong.tools.responses"
local http = require "resty.http"
local json = require "cjson"
local constants = require "kong.constants"
local jwt_decoder = require "kong.plugins.jwt-api-key-plugin.jwt_parser"
local req_get_method = ngx.req.get_method

function preg_replace(pat,with,p)
  return (string.gsub(p,pat,with))
end

function dumpvar(data)
    -- cache of tables already printed, to avoid infinite recursive loops
    local tablecache = {}
    local buffer = ""
    local padder = "    "

    local function _dumpvar(d, depth)
        local t = type(d)
        local str = tostring(d)
        if (t == "table") then
            if (tablecache[str]) then
                -- table already dumped before, so we dont
                -- dump it again, just mention it
                buffer = buffer.."<"..str..">\n"
            else
                tablecache[str] = (tablecache[str] or 0) + 1
                buffer = buffer.."("..str..") {\n"
                for k, v in pairs(d) do
                    buffer = buffer..string.rep(padder, depth+1).."["..k.."] => "
                    _dumpvar(v, depth+1)
                end
                buffer = buffer..string.rep(padder, depth).."}\n"
            end
        elseif (t == "number") then
            buffer = buffer.."("..t..") "..str.."\n"
        else
            buffer = buffer.."("..t..") \""..str.."\"\n"
        end
    end
    _dumpvar(data, 0)
    return buffer
end


-- Setting auth group priority
CouchDbChangerHandler.PRIORITY = 1000

local HTTPS = "https"

-- Parse url based on http-log plugin
local function parse_url(host_url)
  local parsed_url = url.parse(host_url)
  if not parsed_url.port then
    if parsed_url.scheme == "http" then
      parsed_url.port = 80
     elseif parsed_url.scheme == HTTPS then
      parsed_url.port = 443
     end
  end
  if not parsed_url.path then
    parsed_url.path = "/"
  end
  return parsed_url
end

-- Retrieve a JWT in a request.
-- Checks for the JWT in URI parameters, then in the `Authorization` header.
-- @param request ngx request object
-- @param conf Plugin configuration
-- @return token JWT token contained in request or nil
-- @return err
local function retrieve_apikey(request, conf)

  local apikey_header = request.get_headers()["x-apikey"]
  print(request.get_headers()["x-apikey"])
  if apikey_header then
    return apikey_header
  end

  local uri_parameters = request.get_uri_args()

  for _, v in ipairs(conf.uri_param_names) do
    if uri_parameters[v] then
      return uri_parameters[v]
    end
  end

  local authorization_header = request.get_headers()["authorization"]
  if authorization_header then
    local iterator, iter_err = ngx_re_gmatch(authorization_header, "\\s*[Bb]earer\\s+(.+)")
    if not iterator then
      return nil, iter_err
    end

    local m, err = iterator()
    if err then
      return nil, err
    end

    if m and #m > 0 then
      return m[1]
    end
  end
end

-- Your plugin handler's constructor. If you are extending the
-- Base Plugin handler, it's only role is to instanciate itself
-- with a name. The name is your plugin name as it will be printed in the logs.
function CouchDbChangerHandler:new()
  CouchDbChangerHandler.super.new(self, "couchdb-db-changer-plugin")
end

function CouchDbChangerHandler:init_worker(config)
  -- Eventually, execute the parent implementation
  -- (will log that your plugin is entering this context)
  CouchDbChangerHandler.super.init_worker(self)

  -- Access to configuration values
  -- print(config.jwt_server_url) -- {"jwt_server_url"}

  -- Implement any custom logic here
end

function CouchDbChangerHandler:certificate(config)
  -- Eventually, execute the parent implementation
  -- (will log that your plugin is entering this context)
  CouchDbChangerHandler.super.certificate(self)

  -- Implement any custom logic here
end

function CouchDbChangerHandler:access(config)
  -- (will log that your plugin is entering this context)
  CouchDbChangerHandler.super.access(self)

  -- Handling OPTIONS Cors preflight
   if req_get_method() == "OPTIONS" then
       -- ngx.header["Access-Control-Allow-Origin"] = "*"
       local req_origin = ngx.var.http_origin
       if req_origin then
           ngx.header["Access-Control-Allow-Origin"] = req_origin
           ngx.header["Vary"] = "Origin"
       end
       ngx.header["Access-Control-Allow-Credentials"] = "true"
       ngx.header["Access-Control-Allow-Headers"] = "X-Apikey, Content-type"
       ngx.header["Access-Control-Allow-Methods"] = "GET,HEAD,PUT,PATCH,POST,DELETE"
       return responses.send_HTTP_NO_CONTENT()
   end

  -- Only doing rewrite if the whitelist matches
  ngx.log(ngx.NOTICE, "Path matched whitelist")

  local ok, err
  local parsed_url = parse_url(config.jwt_server_url)

  -- Getting apikey from querystring or authorization headers
  local apikey, err = retrieve_apikey(ngx.req, config)

  if err then
      return responses.send_HTTP_INTERNAL_SERVER_ERROR(err)
  end
  -- No apikey so unauthorize request
  if not apikey then
      return responses.send_HTTP_UNAUTHORIZED()
  end

  local host = parsed_url.host
  local port = tonumber(parsed_url.port)

  local endpoint = config.jwt_server_url .. "?apikey=" .. apikey
  local httpc = http.new()
  print(endpoint)
  local res, err = httpc:request_uri(endpoint, {
      method = "GET",
      headers = ngx.req.headers,
  })

  if err then
      print(err)
      ngx.log(ngx.ERR, "An error has occured: ", err)
  end

  if not res then
      print('No res')
      ngx.log(ngx.ERR, "An error has occured: ", err)
      return responses.send_HTTP_UNAUTHORIZED()
  end

  if res.status == 200 then
    ngx.log(ngx.INFO, "JWT: ", res.body)
    local resp = json.decode(res.body)
    local ok, err = http:close()

    -- Decode token to find out who the consumer is
    print(resp['token'])
    local jwt, err = jwt_decoder:new(resp['token'])

    if err then
      print(tostring(err))
      return responses.send_HTTP_UNAUTHORIZED()
    end

    if jwt then
        local claims = jwt.claims
        local account = claims['defaultAccount']

        if account then
          ngx.req.set_uri(preg_replace('members', "members" .. "_" .. account, ngx.var.uri))
          ngx.req.set_uri(preg_replace('transactions', "members" .. "_" .. account, ngx.var.uri))
          local credentials = ngx.encode_base64(config.couch_user .. ":" .. config.couch_pass)
          ngx.req.set_header("Authorization", "Basic " .. credentials)
          local req_origin = ngx.var.http_origin
          if req_origin then
              ngx.header["Access-Control-Allow-Origin"] = req_origin
              ngx.header["Vary"] = "Origin"
          end
          ngx.header["Access-Control-Allow-Credentials"] = "true"

        end
    end

    if err then
      return false, {status = 401, message = "Bad token; "..tostring(err)}
    end
  end
end

function CouchDbChangerHandler:header_filter(config)
  -- Eventually, execute the parent implementation
  -- (will log that your plugin is entering this context)
  CouchDbChangerHandler.super.header_filter(self)

  -- Implement any custom logic here
end

function CouchDbChangerHandler:body_filter(config)
  -- Eventually, execute the parent implementation
  -- (will log that your plugin is entering this context)
  CouchDbChangerHandler.super.body_filter(self)

  -- Implement any custom logic here
end

function CouchDbChangerHandler:log(config)
  -- Eventually, execute the parent implementation
  -- (will log that your plugin is entering this context)
  CouchDbChangerHandler.super.log(self)

  -- Implement any custom logic here
end

-- This module needs to return the created table, so that Kong
-- can execute those functions.
return CouchDbChangerHandler

