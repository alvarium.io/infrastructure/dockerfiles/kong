return {
  no_consumer = true, -- this plugin will only be API-wide,
  fields = {
    -- Describe your plugin's configuration's schema here.
    jwt_server_url = {type = "string", required = true, default = "http://auth.cfs.cirici.com/apiToken"},
    timeout = {type = "string", required = true, default = "30"},
    uri_param_names = {type = "array", default = {"apikey"}},
    path_rename_whitelist= {type = "array", default = {}},
    couchdb_server_url = {type = "string", required = true, default = "http://couch01.alvarium.io"},
    couch_user= {type = "string", default = ""},
    couch_pass= {type = "string", default = ""}
  },
  self_check = function(schema, plugin_t, dao, is_updating)
    -- perform any custom verification
    return true
  end
}
